
public aspect TraceAspect_frederes {
	pointcut classToTrace(): within(*App);
	
	pointcut getMethodToTrace(): classToTrace() && execution(* *(..));
	
	before(): getMethodToTrace() {
		String info = thisJoinPointStaticPart.getSignature() + " " + thisJoinPointStaticPart.getSourceLocation().getLine();
		System.out.println("[BGN] " + info);
	}
	
	after(): getMethodToTrace() {
		String fileName = thisJoinPointStaticPart.getSourceLocation().getFileName();
		System.out.println("[END] " + fileName);
		
	}
		

}
